﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EjerciciosLinQ
{
    class Primer_Ejercicio
    {
        //Se crea una lista de 20 números
        List<int> Numeros = new List<int> { 15, 5, 83, 93, 19, 44, 10, 19, 55, 28, 50, 12, 35, 46, 95, 76, 51, 87, 60, 20 };

        //Se mostrarán solo los números primos
        public void MostrarNumPrimos()
        {
           
            int numero = 2, divisible = 0; 
            var primo = from number in Numeros
                             select number;

                while (numero<=100)
                {
                    for (int i = 1; i <= numero; i++)
                    {
                        if (numero%i==0)
                        {
                            divisible++;
                        }
                        if(divisible>2)
                        {
                            break;
                        }
                    }
                    if (divisible == 2)
                    {
                        foreach (var number in primo)
                        {
                        if (numero == number)
                        {
                            Console.WriteLine(" " + numero);
                        }
                        }
                    }
                    divisible = 0;
                    numero++;
                }
        }

        //Permitirá mostrar la suma de todos los elementos de la lista
        public void SumaElementos()
        {
            IEnumerable<int> sumelementos = from number in Numeros
                                      select number;
            var totalsuma = sumelementos.Sum();

            Console.WriteLine("La Suma de los números es: \n " + totalsuma);
        }

        //Se mostrará el cuadrado de cada número de la lista
        public void CuadradoDeCadaNúmero()
        {
            IEnumerable<int> cuadrado = from number in Numeros
                                        select number;

            foreach  (int number in cuadrado)
            {
                int resultado = number * number;
                Console.WriteLine(" " + number + " al cuadrado es: " + resultado);
            }
        }

        //Se realizará una nueva lista que permitirá ver sólo los números primos
        public void ListaNumPrimos()
        {
            MostrarNumPrimos();
        }

        //Se calcula el promedio de todos los números mayores a 50
        public void CalculoPromedio()
        {
            var average = from number in Numeros
                          where number > 50
                          select number;

            var promedio = average.Average();

            foreach (int number in average)
            {
                Console.WriteLine(number);
            }

            Console.WriteLine(" ");
            Console.WriteLine("El promedio de los números mayores a 50 es: \n " + promedio);

        }

        //Se contará y mostrará la cantidad de números pares e impares
        public void NumPareseImpares()
        {

            var paresimpares = from number in Numeros
                               where number % 2 == 0
                               select number;
            var cantidadpares = paresimpares.Count();
            Console.Write("La cantidad de Pares es: \n " + cantidadpares + " \n ");

            int cantidadimpares = 20 - cantidadpares;
            Console.WriteLine("La cantidad de Impares es: \n " + cantidadimpares);

        }

        //Se mostrará el número y la cantidad de veces que este se encuentra en la lista.
        public void NumeroEnLista()
        {
            var repetidos = from number in Numeros
                            group number by number into gruporepetidos
                            select gruporepetidos;

            foreach (var grupo in repetidos)
            {
                if (grupo.Count() == 1)
                {
                } 
                else
                {
                    Console.WriteLine(" " + grupo.Key + " se repite " + grupo.Count() + " veces");
                }
            }
        }

        //Se mostrarán los elementos de la lista de forma descendente
        public void OrdenDescendente()
        {
            var descendente = from number in Numeros
                              orderby number descending
                              select number;
            foreach (int number in descendente)
            {
                Console.WriteLine(" " + number);
            }
        }

        //Se mostrarán los números únicos en la lista
        public void NumerosUnicos()
        {
            var unicos = from number in Numeros
                         group number by number into grupo
                         where grupo.Count() == 1
                         select grupo;

            foreach (var grupo in unicos)
            {
                Console.WriteLine(" " + grupo.Key);
            }
        }

        //Se suman todos los números únicos de la lista
        public void SumaNumUnicos()
        {
            int sumadeunicos = 0;

            var unicos = from number in Numeros
                         group number by number into grupo
                         where grupo.Count() == 1
                         select grupo;

            foreach (var grupo in unicos)
            {
                sumadeunicos = sumadeunicos + grupo.Key;
            }
            Console.WriteLine(" " + sumadeunicos);
        }
    }
}
