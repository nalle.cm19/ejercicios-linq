﻿using System;
using System.Linq;

namespace EjerciciosLinQ
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion = 0;
            do
            {
                //Se crea un menú de inicio para tener una presentación amigable con el usuario
                //Seleccionando una opción de las correctas el usuario podrá decidir si ver el programa números o el programa factura
                Console.Clear();
                Console.WriteLine(" - MENÚ DE INICIO - ");
                Console.WriteLine(
                    "\n1.- Ejecutar Ejercicio # 1" +
                    "\n2.- Ejecutar Ejercicio # 2" +
                    "\n3.- Salir \n");
                Console.Write("Digite una opción: \n" +
                    " -> "); opcion = int.Parse(Console.ReadLine());
                //Se utiliza switch para poder cumplir lo mencionado anterior mente
                switch (opcion)
                {
                    case 1:
                        Console.Clear();

                        Primer_Ejercicio primerejercicio = new Primer_Ejercicio();
                        Console.WriteLine("Los siguientes números son primos: \n");
                        primerejercicio.MostrarNumPrimos();

                        Console.ReadKey();
                        Console.Clear();

                        primerejercicio.SumaElementos();

                        Console.ReadKey();
                        Console.Clear();

                        Console.WriteLine("El cuadrado de cada número: \n");
                        primerejercicio.CuadradoDeCadaNúmero();

                        Console.ReadKey();
                        Console.Clear();

                        Console.WriteLine("Nueva lista de números primos: \n");
                        primerejercicio.ListaNumPrimos();

                        Console.ReadKey();
                        Console.Clear();

                        Console.WriteLine("Números ordenados de manera descendente: \n");
                        primerejercicio.OrdenDescendente();

                        Console.ReadKey();
                        Console.Clear();

                        Console.WriteLine("Los números mayores a 50 son: \n");
                        primerejercicio.CalculoPromedio();

                        Console.ReadKey();
                        Console.Clear();

                        primerejercicio.NumPareseImpares();

                        Console.ReadKey();
                        Console.Clear();

                        Console.WriteLine("Números que se repiten: \n");
                        primerejercicio.NumeroEnLista();

                        Console.ReadKey();
                        Console.Clear();

                        Console.WriteLine("Los números únicos son: \n");
                        primerejercicio.NumerosUnicos();

                        Console.ReadKey();
                        Console.Clear();

                        Console.WriteLine("La suma de los números únicos es: \n");
                        primerejercicio.SumaNumUnicos();

                        Console.Clear();
                        Console.WriteLine("La ejecución ha finalizado");

                        break;

                    case 2:
                        Console.Clear();

                        //Primero se podrá visualizar el monto mayor de ventas
                        Segundo_Ejercicio segundoejercicio = new Segundo_Ejercicio();
                        Console.WriteLine("Clientes con mayor monto en ventas: \n");
                        segundoejercicio.MayorMonto();

                        Console.ReadKey();
                        Console.Clear();

                        //Se podrá observar el menor monto de ventas
                        Console.WriteLine("Clientes con menor monto en ventas: \n");
                        segundoejercicio.MenorMonto();

                        Console.ReadKey();
                        Console.Clear();

                        //Se visualizará el cliente con el mayor número de ventas
                        Console.WriteLine("Cliente con mayor número de ventas: \n");
                        segundoejercicio.Mayorventas();

                        Console.ReadKey();
                        Console.Clear();

                        //Permitirá observar el nombre de los clientes y las ventas realizadas
                        Console.WriteLine("Nombre de los cliente y el número de ventas realizados por cada uno: \n");
                        segundoejercicio.VentasDelCliente();

                        Console.ReadKey();
                        Console.Clear();

                        //Se visualizarán las ventas realizadas hace menos de un año
                        Console.WriteLine("Ventas realizadas hace menos de un año: \n");
                        segundoejercicio.VentasHaceMenosDeUnAño();

                        Console.ReadKey();
                        Console.Clear();

                        //Se visualizarán las ventas más antiguas
                        Console.WriteLine("Ventas realizadas hace un año o más: \n");
                        segundoejercicio.VentasMásAntiguas();

                        Console.ReadKey();
                        Console.Clear();

                        //Se podrán visualizar los clientes cuyas las observaciones comiencen con Prob
                        Console.WriteLine("Clientes cuyas observaciones de venta comienzan con 'Prob': \n");
                        segundoejercicio.ObservacionProb();

                        Console.Clear();
                        Console.WriteLine("La ejecución ha finalizado");

                        break;
                        //Caso contrario se muestra un mensaje de despedida

                    case 3:

                        Console.Clear();
                        Console.WriteLine("  - Hasta Pronto ... -");

                        break;

                    default:
                        //Si se coloca una opción incorrecta aparecerá este mensaje
                        Console.Clear();
                        Console.WriteLine("Vuelve a intentarlo, pero ahora con una opción valida");

                        break;
                }
                Console.ReadKey();

            } while (opcion != 3);
        }
    }
}
