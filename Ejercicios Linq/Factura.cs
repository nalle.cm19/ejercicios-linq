﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjerciciosLinQ
{
    class Factura
    {
        //Se declaran las variables
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Observacion { get; set; }
        public int idCliente { get; set; }
        public DateTime fecha { get; set; }
        public double total { get; set; }
        public int NumVentas { get; set; }
        public string Ciudad { get; set; }
        public int CantObjetosComprados { get; set; }
        public int ventasrealizadas { get; set; }
        public string NumCaja { get; set; }
        public int NumDeFactura { get; set; }

        public void DatosDeFactura()
        {
            //Regreso de datos
            Console.WriteLine("Nombre: {0} \n" +
                "Apellido: {1} \n" +
                "Observación: {2} \n" +
                "idCliente: {3} \n" +
                "Fecha: {4} \n" +
                "Total: {5} \n" +
                "Monto en Ventas: {6} \n" +
                "Ciudad: {7} \n" +
                "Número de Objetos Comprados: {8} \n" +
                "Numero de ventas realizadas: {9} \n" +
                "Número de caja: {10} \n" +
                "Numero de Factura: {11} \n" +
                Nombre, Apellido, Observacion, idCliente, fecha, total, NumVentas, Ciudad, CantObjetosComprados, ventasrealizadas, NumCaja, NumDeFactura);
        }
    }
}
