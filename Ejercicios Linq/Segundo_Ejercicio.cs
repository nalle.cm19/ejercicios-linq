﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EjerciciosLinQ
{
    class Segundo_Ejercicio
    {
        public List<Clientes> ListaClientes;
        public List<Factura> ListaFacturas;

        public Segundo_Ejercicio()
        {
            //Lista de clientes

            ListaClientes = new List<Clientes>();
            ListaClientes.Add(new Clientes { idCliente= 1, Nombre=" Jeffersson",Apellido="Mora",NumCelular= 0987326153, Edad= 20,Sexo= "Masculino",
                Correo= "JeffitoM01@gmail.com", Cedula= 1306473821});
            ListaClientes.Add(new Clientes { idCliente = 2, Nombre = " Aurora", Apellido = "Palma", NumCelular = 0986046228, Edad = 21, Sexo = " Femenino", 
                Correo = "apalma022000@gmail.com", Cedula =1314443181});
            ListaClientes.Add(new Clientes { idCliente = 3, Nombre = " Carlos", Apellido = "Alonzo", NumCelular = 0981222395, Edad = 22, Sexo = "Masculino ", 
                Correo = "alonzo10ca@gmail.com ", Cedula = 1314443181});
            ListaClientes.Add(new Clientes { idCliente = 4, Nombre = " Yuleidy", Apellido = "Lucas", NumCelular = 0987740489, Edad = 19 , Sexo = "Masculinoo", 
                Correo = "lhyuleidy05@gmail.com", Cedula = 1314443181});
            ListaClientes.Add(new Clientes { idCliente = 5, Nombre = "Tamara", Apellido = "Alarcón", NumCelular = 0996894251, Edad = 20, Sexo = "Femenino ", 
                Correo = "moretamya@gmail.com", Cedula =1314443181});
            ListaClientes.Add(new Clientes { idCliente = 6, Nombre = " Anthony", Apellido = "Chancay", NumCelular = 0987449016, Edad = 23, Sexo = "Masculino", 
                Correo = "anth01ony@gmail.com", Cedula = 1316047381});
            ListaClientes.Add(new Clientes { idCliente = 7, Nombre = "Luis", Apellido = "Zambrano", NumCelular = 0998602667, Edad = 25, Sexo = "Masculino", 
                Correo = "zambranolla@gmail.com", Cedula = 1350668713});
            ListaClientes.Add(new Clientes { idCliente = 8, Nombre = "Paul", Apellido = "Quijije", NumCelular = 0989006217, Edad = 24, Sexo = " Masculino", 
                Correo = "quijijemp@gmail.com", Cedula = 1306087489});
            ListaClientes.Add(new Clientes { idCliente = 9, Nombre = "Diego", Apellido = "López", NumCelular = 0998865067, Edad = 32, Sexo = " Masculino", 
                Correo = " Diegoblackl@gmail.com", Cedula = 1316073162});
            ListaClientes.Add(new Clientes { idCliente = 10, Nombre = " Alicia", Apellido = "Manchado", NumCelular = 0983359472, Edad = 28, Sexo = " Femenino", 
                Correo = "manchadoali@gmail.com", Cedula = 1308447605});

            //Lista de factura
            ListaFacturas = new List<Factura>();
            ListaFacturas.Add(new Factura { Nombre="Jeffersson",Apellido="Mora",Observacion="Venta realizada con éxito",idCliente=1,fecha = new DateTime(2021, 08, 22) ,total = 542.4 ,NumVentas= 30,
                Ciudad= "Manta",CantObjetosComprados= 15, ventasrealizadas= 16,NumCaja= "Caja #3",NumDeFactura= 100});
            ListaFacturas.Add(new Factura { Nombre = "Aurora", Apellido = "Palma",Observacion="Venta exitosa",idCliente=2,fecha = new DateTime(2020, 06, 21), total= 676.4 ,NumVentas= 50,
                Ciudad= "Manta",CantObjetosComprados= 10, ventasrealizadas= 8,NumCaja= "Caja #2",NumDeFactura= 101});
            ListaFacturas.Add(new Factura { Nombre = "Carlos", Apellido = "Alonzo",Observacion="Venta realizada con éxito",idCliente=3,fecha = new DateTime(2019, 07, 02), total= 989.3 ,NumVentas= 130, 
                Ciudad= "Manta",CantObjetosComprados= 11, ventasrealizadas= 4,NumCaja= "Caja #4",NumDeFactura= 102});
            ListaFacturas.Add(new Factura { Nombre = "Yuleidy", Apellido = "Lucas",Observacion="Probablemente existan errores de impresión",idCliente=4,fecha = new DateTime(2021, 02, 12), total= 129.5 ,NumVentas= 200, 
                Ciudad= "Manta",CantObjetosComprados= 8, ventasrealizadas= 5,NumCaja= "Caja #3",NumDeFactura= 103});
            ListaFacturas.Add(new Factura { Nombre = "Tamara", Apellido = "Alarcón",Observacion="Venta realizada con éxito",idCliente=5,fecha = new DateTime(2021, 05, 31), total= 438.9 ,NumVentas= 77, 
                Ciudad= "Manta",CantObjetosComprados= 4, ventasrealizadas= 4,NumCaja= "Caja #4",NumDeFactura= 104});
            ListaFacturas.Add(new Factura { Nombre = "Anthony", Apellido = "Chancay",Observacion="Probablemente existan errores de impresión",idCliente=6,fecha = new DateTime(2020, 03, 04), total= 534.3 ,NumVentas= 180, 
                Ciudad= "Manta",CantObjetosComprados=14, ventasrealizadas= 12,NumCaja= "Caja #1",NumDeFactura= 105});
            ListaFacturas.Add(new Factura { Nombre = "Luis", Apellido = "Zambrano",Observacion="Venta éxitosa",idCliente=7,fecha = new DateTime(2019, 02, 11), total= 756.54 ,NumVentas= 157, 
                Ciudad= "Manta",CantObjetosComprados= 12, ventasrealizadas= 7,NumCaja= "Caja #1",NumDeFactura= 106});
            ListaFacturas.Add(new Factura { Nombre = "Paul", Apellido = "Quijije",Observacion="Probablemente existan errores de impresión",idCliente=8,fecha = new DateTime(2020, 06, 10), total= 983.2 ,NumVentas= 40, 
                Ciudad= "Manta",CantObjetosComprados= 11, ventasrealizadas= 5,NumCaja= "Caja #2",NumDeFactura= 107});
            ListaFacturas.Add(new Factura { Nombre = "Diego", Apellido = "López",Observacion="Probablemente existan errores de impresión",idCliente=9,fecha = new DateTime(2021, 07, 24), total= 873.5 ,NumVentas= 160, 
                Ciudad= "Manta",CantObjetosComprados= 13, ventasrealizadas= 4,NumCaja= "Caja #3",NumDeFactura= 108});
            ListaFacturas.Add(new Factura { Nombre = "Alicia", Apellido = "Manchado",Observacion="Venta éxitosa",idCliente=10,fecha = new DateTime(2020, 09, 26),total= 962.5 ,NumVentas= 90, 
                Ciudad= "Manta",CantObjetosComprados= 16, ventasrealizadas= 7,NumCaja= "Caja #4",NumDeFactura= 109});
        }

        //Conocer mayor monto
        public void MayorMonto()
        {
            IEnumerable<Factura> mayormonto = from ventas in ListaFacturas
                             where ventas.NumVentas > 100
                             select ventas;

            foreach (Factura item in mayormonto)
            {
                Console.WriteLine(item.Nombre +" "+ item.Apellido + " tiene un monto de --->  " + item.NumVentas);
            }
        }
        //Conocer menor monto
        public void MenorMonto()
        {
            IEnumerable<Factura> menormonto = from ventas in ListaFacturas
                                              where ventas.NumVentas < 100
                                              select ventas;

            foreach (Factura item in menormonto)
            {
                Console.WriteLine(" " + item.Nombre + " " + item.Apellido + " tiene un monto de --->  " + item.NumVentas);
            }
        }
        //Conocer mayor venta
        public void Mayorventas()
        {
            int cliente=0;
            cliente = ListaFacturas.Max(item => item.ventasrealizadas);
            IEnumerable<Factura> ventasrealizadas = from ventas in ListaFacturas
                                                    where ventas.ventasrealizadas == cliente
                                                    select ventas;

            foreach (Factura item in ventasrealizadas)
            {
                Console.WriteLine("Cliente: " + item.Nombre + " " + item.Apellido + "\nCon " + cliente + " ventas realizadas");
            }
        }
        //Conocer ventas del cliente
        public void VentasDelCliente()
        {
            IEnumerable<Factura> ventasdeclientes = from ventas in ListaFacturas
                                                    select ventas;
            foreach (Factura item in ventasdeclientes)
            {
                Console.WriteLine(" " + item.Nombre + " " + item.Apellido + " con: " + item.ventasrealizadas + " ventas");
            }
        }
        //Conocer las ventas de hace menos de un año
        public void VentasHaceMenosDeUnAño()
        {
            IEnumerable<Factura> ventashacemenosdeunaño = from ventas in ListaFacturas
                                                          where ventas.fecha > new DateTime(2020, 08, 31)
                                                          select ventas;
            foreach (var item in ventashacemenosdeunaño)
            {
                Console.WriteLine("Factura # " + item.NumDeFactura + "   fecha:" + item.fecha);
            }
        }
        //Conocer las ventas más antiguas
        public void VentasMásAntiguas()
        {
            IEnumerable<Factura> ventasmasantiguas = from ventas in ListaFacturas
                                                          where ventas.fecha < new DateTime(2020, 01, 01)
                                                          select ventas;
            foreach (var item in ventasmasantiguas)
            {
                Console.WriteLine("Factura # " + item.NumDeFactura + "   fecha:" + item.fecha);
            }
        }
        //Visualizar las Observaciones que comiencen con Prob
        public void ObservacionProb()
        {
            IEnumerable<Factura> observacion = from ventas in ListaFacturas
                                              where ventas.Observacion == "Probablemente existan errores de impresión"
                                              select ventas;

            foreach (Factura item in observacion)
            {
                Console.WriteLine("Cliente -> " + item.Nombre + " " + item.Apellido + "    Observación -> " + item.Observacion);
            }
        }
    }
}
