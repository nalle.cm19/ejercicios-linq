﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjerciciosLinQ
{
    class Clientes
    {
        //Se declaran las variables
        public int idCliente { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int NumCelular { get; set; }
        public int Edad { get; set; }
        public string Sexo { get; set; }
        public int Cedula { get; set; }
        public string Dirección { get; set; }
        public string Correo { get; set; }

        public void DatosDelCliente()
        {
            Console.WriteLine("idCliente: {0} \n" +
                "Nombre: {1} \n" +
                "Apellido: {2} \n" +
                "Número Celular: {3} \n" +
                "Edad: {4} \n" +
                "Sexo: {5} \n" +
                "Cédula: {6} \n" +
                "Dirección: {7} \n" +
                "Correo electrónico: {8} \n" +
                idCliente, Nombre, Apellido, NumCelular, Edad, Sexo, Correo, Cedula);
        }
    }
}
